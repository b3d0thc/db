<?php

    include("db.php");

    if(isset($_GET['nit'])){
        $nt = $_GET['nit'];
        $query = "SELECT * FROM proveedor WHERE nit=$nt";
        $result = mysqli_query($conectar, $query);
        if(mysqli_num_rows($result) == 1){
            $row = mysqli_fetch_array($result);
            $nt = $row['nit'];
            $n = $row['nombre'];
            $di = $row['direccion'];
            $t = $row['telefono'];
            $des = $row['descripcion'];
        }

       
    }

    if(isset($_POST['enviar2'])){
            $nt = $_GET['nit'];
            $n = $_POST['nombre'];
            $di = $_POST['direccion'];
            $t = $_POST['tel'];
            $des = $_POST['descripcion'];

        $c=mysqli_query($conectar,"SELECT * FROM proveedor where nit=$nt");

   	   if(mysqli_num_rows($c) == 0){

            $_SESSION['message'] = 'No se actualizo correctamente';
            $_SESSION['message_type'] = 'danger';
            header("Location: editp.php?nit=$nt");  
   	   	 }
   	   	 else{
                   mysqli_query($conectar,"UPDATE proveedor set nombre='$n', direccion='$di', telefono=$t, descripcion='$des' where nit=$nt");
                   
                   $_SESSION['message'] = 'Se actualizo correctamente';
                    $_SESSION['message_type'] = 'primary';
                   header("Location: proveedor.php");
   	   	 }
    }

?>

<?php include("includes/header.php") ?>

<div class="container p-4">
<div class="row">
<div class="col-md-4 mx-auto">
<div class="card card-body">
<div class="form-label"><h3>ACTUALIZAR PROVEEDOR</h3></div>
    <form action="editp.php?nit=<?php echo $_GET['nit'];?>" method="POST">
    <div class="form-group">
    <input type="text" name="nit" value="<?php echo $nt; ?>" class="form-control" disabled>
    </div>
    <div class="form-group">
    <input type="text" name="nombre" value="<?php echo $n; ?>" class="form-control" placeholder="Actualizar nombre">
    </div>
    <div class="form-group">
    <input type="text" name="direccion" value="<?php echo $di; ?>" class="form-control" placeholder="Actualizar direccion">
    </div>
    <div class="form-group">
    <input type="number" name="tel" value="<?php echo $t; ?>" class="form-control" placeholder="Actualizar telefono">
    </div> 
    <textarea name="descripcion" class="form-control" rows="6"><?php echo $des; ?></textarea>
    </div> <br>
    <div class="d-grid gap-2">
    <input type="submit" class="btn btn-success" name="enviar2" value="ACTUALIZAR">
    </div>
    </form>
</div>
</div>
</div>
</div>

<?php include("includes/footer.php") ?>