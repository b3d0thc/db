<?php

    include("db.php");

    if(isset($_GET['idp'])){
        $idp = $_GET['idp'];
        $query = "SELECT * FROM productos WHERE idp=$idp";
        $result = mysqli_query($conectar, $query);
        if(mysqli_num_rows($result) == 1){
            $row = mysqli_fetch_array($result);
            $idp = $row['idp'];
            $n = $row['nombre'];
            $can = $row['cantidad'];
            $v = $row['valor'];
            $prov = $row['proveedor'];
        }

       
    }

    if(isset($_POST['enviar3'])){
            $idp = $_GET['idp'];
            $n = $_POST['nombre'];
            $can = $_POST['cantidad'];
            $v = $_POST['valor'];
            $prov = $_POST['proveedor'];

        $c=mysqli_query($conectar,"SELECT * FROM productos where idp=$idp");

   	   if(mysqli_num_rows($c) == 0){

            $_SESSION['message'] = 'No se actualizo correctamente';
            $_SESSION['message_type'] = 'danger';
            header("Location: editpr.php?idp=$idp");  
   	   	 }
   	   	 else{
                   mysqli_query($conectar,"UPDATE productos set nombre='$n', cantidad='$can', valor=$v, proveedor='$prov' where idp=$idp");
                   
                   $_SESSION['message'] = 'Se actualizo correctamente';
                    $_SESSION['message_type'] = 'primary';
                   header("Location: producto.php");
   	   	 }
    }

?>

<?php include("includes/header.php") ?>

<div class="container p-4">
<div class="row">
<div class="col-md-4 mx-auto">
<div class="card card-body">
<div class="form-label"><h3>ACTUALIZAR PRODUCTO</h3></div>
    <form action="editpr.php?idp=<?php echo $_GET['idp'];?>" method="POST">
    <div class="form-group">
    <input type="text" name="idp" value="<?php echo $idp; ?>" class="form-control" disabled>
    </div>
    <div class="form-group">
    <input type="text" name="nombre" value="<?php echo $n; ?>" class="form-control" placeholder="Actualizar nombre">
    </div>
    <div class="form-group">
    <input type="text" name="cantidad" value="<?php echo $can; ?>" class="form-control" placeholder="Actualizar cantidad">
    </div>
    <div class="form-group">
    <input type="number" name="valor" value="<?php echo $v; ?>" class="form-control" placeholder="Actualizar valor U.">
    </div> 
    <div class="form-group">
    <input type="number" name="proveedor" value="<?php echo $prov; ?>" class="form-control" placeholder="Actualizar Proveedor">
    </div> 
    <div class="d-grid gap-2">
    <input type="submit" class="btn btn-success" name="enviar3" value="ACTUALIZAR">
    </div>
    </form>
</div>
</div>
</div>
</div>

<?php include("includes/footer.php") ?>