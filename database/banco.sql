-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3307
-- Tiempo de generación: 15-12-2020 a las 05:52:13
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `banco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `documento` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`documento`, `nombre`, `direccion`, `telefono`) VALUES
(111, 'carlos', 'CALLE 39A #10 45', 123123123),
(98764312, 'kane133', 'dawdwad', 2132400),
(111111111, 'dario gomez', 'CALLE 39A #10 45', 496488003),
(1216758680, 'Jonatan sTR', 'CALLE 39A', 95135473),
(1216758685, 'Jonatan Stivens', 'CALLE 39A #10 45', 32412414),
(1222329893, 'carlosTHCCCCC', 'CALLE 39A', 41242000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `n_f` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  `descuento` tinyint(4) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`n_f`, `cliente`, `producto`, `descuento`, `total`) VALUES
(1, 111, 420, 12, 64800),
(2, 111, 520, 11, 88000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idp` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `cantidad` mediumint(9) NOT NULL,
  `valor` int(11) NOT NULL,
  `proveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idp`, `nombre`, `cantidad`, `valor`, `proveedor`) VALUES
(420, 'blanqueador', 4000, 50000, 5468165),
(520, 'escobas', 300, 2500, 5468165),
(4004, 'traperas', 200, 3000, 111),
(23123, 'wfdawfawf', 133, 3333, 354654681),
(80008, 'fabuloso', 100, 7500, 111);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `nit` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `direccion` varchar(70) CHARACTER SET utf8mb4 NOT NULL,
  `telefono` int(13) NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`nit`, `nombre`, `direccion`, `telefono`, `descripcion`) VALUES
(111, 'LIMPIEZA PLUS', 'CALLE 39A #10 45', 4985040, 'Cada mes a las 9 am'),
(5468165, 'fawwgfawg', '1fa65w1f6aw1f', 1681661, 'faw1faw6f16aw1f68wa1f6wa1f6wa1f6aw1f65wa1f651wfawf'),
(354654681, 'fawgwagawgawwgf', '516g1ag', 5165156, 'gakowpjgoijawgoijoigkag');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`documento`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`n_f`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `producto` (`producto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idp`),
  ADD KEY `proveedor` (`proveedor`) USING BTREE;

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`nit`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `n_f` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `facturacion` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`documento`) ON UPDATE CASCADE,
  ADD CONSTRAINT `producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`idp`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `compra_proveedor` FOREIGN KEY (`proveedor`) REFERENCES `proveedor` (`nit`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
