<?php include("db.php") ?>

<?php include("includes/header.php") ?>


  <div class="container p-4">
  <?php if(isset($_SESSION['message'])){ ?>
    <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
    <?= $_SESSION['message'] ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
    <div class="row">
    <div class="col-md-4">
        <div class="card card-body">
            <form action="save_task.php" method="POST">
                <div class="form-label"><h3>FACTURA</h3></div>
                
                <div class="form-group"><input type="number" name="cliente" class="form-control" placeholder="CLIENTE"></div>
                <div class="form-group"><input type="number" name="producto" class="form-control" placeholder="PRODUCTO"></div>
                <div class="form-group"><input type="number" name="descuento" class="form-control" placeholder="DESCUENTO"></div>
                <select class="form-select" aria-label="Default select example">
                  <option selected>Open this select menu</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
                <div class="form-group"><input type="number" name="total" class="form-control" placeholder="Valor total"></div>
                
                <br><div class="d-grid gap-2">
                <input type="submit" class="btn btn-block btn-primary" name="enviar_task4" value="AGREGAR">
                <input type="reset"  class="btn btn-danger btn-primary" value="CANCELAR"></div>
            </form>
        </div>
    </div>

    <div class="col-md-8">
        <table class="table table-bordered">
        <thead>
        <tr>
            <th>Num. Factura</th>
            <th>CLIENTE</th>
            <th>PRODUCTO</th>
            <th>DESCUENTO</th>
            <th>V. TOTAL</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $query = "SELECT * FROM factura";
            $result_task = mysqli_query($conectar, $query);

            while($row = mysqli_fetch_array($result_task)){ ?>

                <tr>
                  <td><?php echo $row['n_f'] ?></td>
                  <td><?php echo $row['cliente'] ?></td>
                  <td><?php echo $row['producto'] ?></td>
                  <td><?php echo $row['descuento'] ?></td>
                  <td><?php echo $row['total'] ?></td>
                  <td>
                    <a href="editf.php?n_f=<?php echo $row['n_f']?>" class="btn btn-warning"><i class="fas fa-user-edit"></i></a>
                    <a href="delete_task.php?nit=<?php echo $row['n_f']?>" class="btn btn-danger"><i class="fas fa-user-times"></i></a>
                  </td>
                </tr>

            <?php } ?>

        </tbody>
        </table>
    </div>
    </div>
  </div>

<?php include("includes/footer.php") ?>
