<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD PERSONALIZADO</title>

        <!-- CDN Frameworks -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
            <!--FontAwesome-->
            <script src="https://kit.fontawesome.com/6ae53a3950.js" crossorigin="anonymous"></script>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a href="index.php" class="navbar-brand">PHP CRUD</a>
      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav me-auto mb-lg-0">
            <li class="nav-item"><a class="nav-link " arial-current="page" href="index.php">INICIO</a></li>
            <li class="nav-item"><a class="nav-link " arial-current="page" href="cliente.php">CLIENTE</a></li>
            <li class="nav-item"><a class="nav-link " arial-current="page" href="proveedor.php">PROVEEDOR</a></li>
            <li class="nav-item"><a class="nav-link " arial-current="page" href="producto.php">PRODUCTO</a></li>
        </ul>
    </div>
</nav>