<?php include("db.php") ?>

<?php include("includes/header.php") ?>


  <div class="container p-4">
  <?php if(isset($_SESSION['message'])){ ?>
    <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
    <?= $_SESSION['message'] ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
    <div class="row">
    <div class="col-md-4">
        <div class="card card-body">
            <form action="save_task.php" method="POST">
                <div class="form-label"><h3>PRODUCTO</h3></div>
                <div class="form-group">
                <input type="number" name="idp" class="form-control" placeholder="PRODUCTO" autofocus>
                </div>
                <div class="form-group"><input type="text" name="nombre" class="form-control" placeholder="NOMBRE DEL PRODUCTO"></div>
                <div class="form-group"><input type="number" name="cantidad" class="form-control" placeholder="CANTIDAD"></div>
                <div class="form-group"><input type="number" name="valor" class="form-control" placeholder="VALOR UNITARIO"></div>
                <select class="form-select" aria-label="Default select example">
                  <option selected>Open this select menu</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
                <div class="form-group"><input type="number" name="nitp" class="form-control" placeholder="NIT DEL PROVEEDOR"></div>
                
                <br><div class="d-grid gap-2">
                <input type="submit" class="btn btn-block btn-primary" name="enviar_task3" value="AGREGAR">
                <input type="reset"  class="btn btn-danger btn-primary" value="CANCELAR"></div>
            </form>
        </div>
    </div>

    <div class="col-md-8">
        <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID PRODUCTO</th>
            <th>NOMBRE</th>
            <th>CANTIDAD</th>
            <th>VALOR</th>
            <th>NIT PROVEEDOR</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $query = "SELECT * FROM productos";
            $result_task = mysqli_query($conectar, $query);

            while($row = mysqli_fetch_array($result_task)){ ?>

                <tr>
                  <td><?php echo $row['idp'] ?></td>
                  <td><?php echo $row['nombre'] ?></td>
                  <td><?php echo $row['cantidad'] ?></td>
                  <td><?php echo $row['valor'] ?></td>
                  <td><?php echo $row['proveedor'] ?></td>
                  <td>
                    <a href="editpr.php?idp=<?php echo $row['idp']?>" class="btn btn-warning"><i class="fas fa-user-edit"></i></a>
                    <a href="delete_task.php?idp=<?php echo $row['idp']?>" class="btn btn-danger"><i class="fas fa-user-times"></i></a>
                  </td>
                </tr>

            <?php } ?>

        </tbody>
        </table>
    </div>
    </div>
  </div>

<?php include("includes/footer.php") ?>