<?php

    include("db.php");

    if(isset($_GET['documento'])){
        $id = $_GET['documento'];
        $query = "DELETE FROM cliente WHERE documento=$id";
        $result = mysqli_query($conectar, $query);
        if(!$result){
            die("No existe el resultado");
        }

        $_SESSION['message'] = 'Se elimino correctamente';
        $_SESSION['message_type'] = 'danger';
        header("Location: cliente.php");
    }

    if(isset($_GET['nit'])){
        $nt = $_GET['nit'];
        $query = "DELETE FROM proveedor WHERE nit=$nt";
        $result = mysqli_query($conectar, $query);
        if(!$result){
            die("No existe el resultado");
        }

        $_SESSION['message'] = 'Se elimino correctamente';
        $_SESSION['message_type'] = 'danger';
        header("Location: proveedor.php");
    }

    if(isset($_GET['idp'])){
        $idp = $_GET['idp'];
        $query = "DELETE FROM productos WHERE idp=$idp";
        $result = mysqli_query($conectar, $query);
        if(!$result){
            die("No existe el resultado");
        }

        $_SESSION['message'] = 'Se elimino correctamente';
        $_SESSION['message_type'] = 'danger';
        header("Location: producto.php");
    }

?>