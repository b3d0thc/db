<?php include("db.php") ?>

<?php include("includes/header.php") ?>


  <div class="container p-4">
  <?php if(isset($_SESSION['message'])){ ?>
    <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
    <?= $_SESSION['message'] ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
    <div class="row">
    <div class="col-md-4">
        <div class="card card-body">
            <form action="save_task.php" method="POST">
                <div class="form-label"><h3>CLIENTEE</h3></div>
                <div class="form-group">
                <input type="number" name="doc" class="form-control" placeholder="Documento" autofocus>
                </div>
                <div class="form-group"><input type="text" name="nombre" class="form-control" placeholder="Nombre Completo"></div>
                <div class="form-group"><input type="text" name="direc" class="form-control" placeholder="Direccion"></div>
                <div class="form-group"><input type="number" name="tel" class="form-control" placeholder="Telefono"></div>
                <select class="form-select" aria-label="Default select example">
                  <option selected>Open this select menu</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
                <br>
                <label for="exampleColorInput" class="form-label">Color picker</label>
<input type="color" class="form-control form-control-color" id="exampleColorInput" value="#563d7c" title="Choose your color">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                  <label class="form-check-label" for="flexRadioDefault1">
                    Default radio
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                  <label class="form-check-label" for="flexRadioDefault2">
                    Default checked radio
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                  <label class="form-check-label" for="inlineCheckbox1">1</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                  <label class="form-check-label" for="inlineCheckbox2">2</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" disabled>
                  <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                </div>
                
                <br><div class="d-grid gap-2">
                <input type="submit" class="btn btn-block btn-primary" name="enviar_task" value="AGREGAR">
                <input type="reset"  class="btn btn-danger btn-primary" value="CANCELAR"></div>
            </form>
        </div>
    </div>

    <div class="col-md-8">
        <table class="table table-bordered">
        <thead>
        <tr>
            <th>DOCUMENTO</th>
            <th>NOMBRE</th>
            <th>DIRECCION</th>
            <th>TELEFONO</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $query = "SELECT * FROM cliente ORDER BY documento ASC";
            $result_task = mysqli_query($conectar, $query);

            while($row = mysqli_fetch_array($result_task)){ ?>

                <tr>
                  <td><?php echo $row['documento'] ?></td>
                  <td><?php echo $row['nombre'] ?></td>
                  <td><?php echo $row['direccion'] ?></td>
                  <td><?php echo $row['telefono'] ?></td>
                  <td>
                    <a href="edit.php?documento=<?php echo $row['documento']?>" class="btn btn-warning"><i class="fas fa-user-edit"></i></a>
                    <a href="delete_task.php?documento=<?php echo $row['documento']?>" class="btn btn-danger"><i class="fas fa-user-times"></i></a>
                  </td>
                </tr>

            <?php } ?>

        </tbody>
        </table>
    </div>
    </div>
  </div>

<?php include("includes/footer.php") ?>
