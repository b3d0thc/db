<?php

    include("db.php");

    if(isset($_GET['documento'])){
        $id = $_GET['documento'];
        $query = "SELECT * FROM cliente WHERE documento=$id";
        $result = mysqli_query($conectar, $query);
        if(mysqli_num_rows($result) == 1){
            $row = mysqli_fetch_array($result);
            $d = $row['documento'];
            $n = $row['nombre'];
            $di = $row['direccion'];
            $t = $row['telefono'];
        }

       
    }

    if(isset($_POST['enviar'])){
        $d = $_GET['documento'];
            $n = $_POST['nombre'];
            $di = $_POST['direccion'];
            $t = $_POST['tel'];

        $c=mysqli_query($conectar,"SELECT * FROM cliente where documento=$d");

   	   if(mysqli_num_rows($c) == 0){

            $_SESSION['message'] = 'No se actualizo correctamente';
            $_SESSION['message_type'] = 'danger';
            header("Location: edit.php?documento=$d");  
   	   	 }
   	   	 else{
                   mysqli_query($conectar,"UPDATE cliente set nombre='$n', direccion='$di', telefono=$t where documento=$d");
                   
                   $_SESSION['message'] = 'Se actualizo correctamente';
                    $_SESSION['message_type'] = 'primary';
                   header("Location: cliente.php");
   	   	 }
    }

?>

<?php include("includes/header.php") ?>

<div class="container p-4">
<div class="row">
<div class="col-md-4 mx-auto">
<div class="card card-body">
<div class="form-label"><h3>ACTUALIZAR CLIENTE</h3></div>
    <form action="edit.php?documento=<?php echo $_GET['documento'];?>" method="POST">
    <div class="form-group">
    <input type="text" name="documento" value="<?php echo $d; ?>" class="form-control" disabled>
    </div>
    <div class="form-group">
    <input type="text" name="nombre" value="<?php echo $n; ?>" class="form-control" placeholder="Actualizar nombre">
    </div>
    <div class="form-group">
    <input type="text" name="direccion" value="<?php echo $di; ?>" class="form-control" placeholder="Actualizar direccion">
    </div>
    <div class="form-group">
    <input type="number" name="tel" value="<?php echo $t; ?>" class="form-control" placeholder="Actualizar telefono">
    </div> 
     <br>
    <div class="d-grid gap-2">
    <input type="submit" class="btn btn-success" name="enviar" value="ACTUALIZAR">
    </div>
    </form>
</div>
</div>
</div>
</div>

<?php include("includes/footer.php") ?>